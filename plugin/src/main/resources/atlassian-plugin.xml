<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">

    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="plugin-icon">icon.png</param>
        <param name="plugin-logo">logo.png</param>
    </plugin-info>
  
    <resource type="i18n" name="MercurialActionSupport" location="net.customware.jira.plugins.ext.mercurial.action.MercurialActionSupport" />
    <resource type="i18n" name="reports-mercurial_jira_plugin" location="templates.plugins.mercurial.reports.reports"/>
    <resource type="i18n" name="projecttabpanels-mercurial_jira_plugin" location="templates.plugins.mercurial.projecttabpanels.mercurial_jira_plugin"/>
    <resource type="i18n" name="issuetabpanels-mercurial_jira_plugin" location="templates.plugins.mercurial.issuetabpanels.mercurial_jira_plugin"/>

    <issue-tabpanel key="mercurial-commits-tabpanel" name="Mercurial Changes Tab Panel" class="net.customware.jira.plugins.ext.mercurial.issuetabpanels.changes.MercurialRevisionsTabPanel">
        <description>Show Mercurial commits related to an issue in an issue tab panel.</description>
        <label key="mercurial.issue-tab.label" />
        <resource type="velocity" name="view" location="templates/plugins/mercurial/issuetabpanels/mercurial-commits-view.vm"/>
    </issue-tabpanel>

    <project-tabpanel key="mercurial-project-tab" name="Mercurial Commits Project Tab" class="net.customware.jira.plugins.ext.mercurial.projecttabpanels.MercurialProjectTabPanel">
        <description key="mercurial.project-tab.description"/>
        <label key="mercurial.project-tab.label" />
        <!-- this is a number defining the order of all project tabs. The system panels are 10, 20, 30 and 40. -->
        <order>51</order>
        <!-- this template produces the HTML for the panel -->
        <resource type="velocity" name="view" location="templates/plugins/mercurial/projecttabpanels/mercurial-project-tab.vm"/>
    </project-tabpanel>

    <project-tabpanel key="mercurial-project-releasenotes-tab" name="Mercurial Releases" class="net.customware.jira.plugins.ext.mercurial.projecttabpanels.MercurialProjectReleaseNotesTabPanel">
        <description key="mercurial.project-release-notes-tab.description">Release notes generated using Mercurial changesets.</description>
        <label key="mercurial.project-release-notes-tab.label" />
        <!-- Keep this next to the mercurial-project-tab -->
        <order>52</order>
        <resource type="velocity" name="view" location="templates/plugins/mercurial/projecttabpanels/mercurial-project-release-notes-tab.vm"/>
    </project-tabpanel>
    
    <web-resource key="mercurial-resource-js" name="Mercurial JavaScript">
        <dependency>jira.webresources:jira-global</dependency>
        <resource type="download" name="mercurial.css" location="templates/plugins/mercurial/css/mercurial.css"/>
        <resource type="download" name="mercurial.js" location="templates/plugins/mercurial/scripts/mercurial.js"/>
    </web-resource>

    <!--
        Automatically finds all JAX-RS resource classes in the plugin and
        publishes them.
     -->
    <rest key="reports-resources" path="/ct" version="1.0" name="Reports Resources">
        <description>Provides the REST resources for reports.</description>
    </rest>

    <!--
        Imports the SAL classes from JIRA so it can be used by this plugin
     -->
    <component-import key="userManager" name="User Manager" interface="com.atlassian.sal.api.user.UserManager"/>
    <component-import key="applicationProperties" interface="com.atlassian.sal.api.ApplicationProperties"/>

    <webwork1 key="MercurialRepositoriesManagement" name="Mercurial repositories management" class="java.lang.Object">
        <actions>
            <action name="net.customware.jira.plugins.ext.mercurial.action.ViewMercurialRepositoriesAction" alias="ViewMercurialRepositories">
                <view name="success">/templates/plugins/mercurial/configuration/list.vm</view>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.AddMercurialRepositoryAction" alias="AddMercurialRepository">
                <view name="input">/templates/plugins/mercurial/configuration/add.vm</view>
                <view name="error">/templates/plugins/mercurial/configuration/add.vm</view>
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>

                <command name="addsubrepos" alias="AddMercurialSubrepositories">
                    <view name="input">/templates/plugins/mercurial/configuration/add_subrepos.vm</view>
                    <view name="error">/templates/plugins/mercurial/configuration/add_subrepos.vm</view>
                </command>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.UpdateMercurialRepositoryAction" alias="UpdateMercurialRepository">
                <view name="input">/templates/plugins/mercurial/configuration/edit.vm</view>
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>
                <view name="error">/templates/plugins/mercurial/configuration/edit.vm</view>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.DeleteMercurialRepositoryAction" alias="DeleteMercurialRepository">
                <view name="input">/templates/plugins/mercurial/configuration/delete.vm</view>
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>
                <view name="error">/templates/plugins/mercurial/configuration/delete.vm</view>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.ActivateMercurialRepositoryAction" alias="ActivateMercurialRepository">
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>
                <view name="success">/templates/plugins/mercurial/configuration/activate.vm</view>
                <view name="all">/templates/plugins/mercurial/configuration/activateall.vm</view>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.DeactivateMercurialRepositoryAction" alias="DeactivateMercurialRepository">
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>
                <view name="success">/templates/plugins/mercurial/configuration/deactivate.vm</view>
                <view name="all">/templates/plugins/mercurial/configuration/deactivateall.vm</view>
            </action>
            
            <action name="net.customware.jira.plugins.ext.mercurial.action.BulkChangeMercurialRepositoriesAction" alias="BulkChangeMercurialRepositories">
                <view name="permissionviolation">/templates/plugins/mercurial/configuration/no-privilege.vm</view>
                <view name="activateall">/templates/plugins/mercurial/configuration/activateall.vm</view>
                <view name="deactivateall">/templates/plugins/mercurial/configuration/deactivateall.vm</view>
                <view name="reindexall">/templates/plugins/mercurial/configuration/reindexall.vm</view>
                <view name="deleteall">/templates/plugins/mercurial/configuration/deleteall.vm</view>
            </action>
        </actions>
    </webwork1>
    
    <web-section key="cwjira-admin" name="CustomWare Plugins" location="admin_plugins_menu" weight="110">
        <label key="net.customware.jira.section.admin.label"/>
        <description key="net.customware.jira.section.admin.desc"/>
        <resource type="i18n" name="i18n" location="net.customware.jira.utils.i18n"/>
        <condition class="net.customware.jira.plugins.ext.mercurial.license.CwWebsectionExist"/>
    </web-section>
    
    <web-item key="mercurial-repositories-link" name="Mercurial repositories link on administrators page" section="admin_plugins_menu/cwjira-admin" weight="31">
        <label key="mercurial.repositories" />
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.JiraGlobalPermissionCondition">
            <param name="permission">admin</param>
        </condition>
        <link linkId="mercurial-repositories">/secure/ViewMercurialRepositories.jspa</link>
    </web-item>

    <resource name="license-generic" type="i18n" location="net.customware.license.jira.v1.i18n"/>
</atlassian-plugin>
