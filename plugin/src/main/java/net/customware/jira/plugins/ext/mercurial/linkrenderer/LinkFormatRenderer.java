package net.customware.jira.plugins.ext.mercurial.linkrenderer;

import net.customware.jira.plugins.ext.mercurial.MercurialConstants;
import net.customware.jira.plugins.ext.mercurial.MercurialManager;
import net.customware.jira.plugins.ext.mercurial.ViewLinkFormat;
import net.customware.hg.core.io.HGLogEntry;
import net.customware.hg.core.io.HGLogEntryPath;

import com.atlassian.core.util.StringUtils;

import org.apache.log4j.Logger;

/**
 * A link renderer implementation which lets the user specify the format in the properties file, to accommodate
 * various formats (ViewCVS, Fisheye, etc) out there.
 *
 * @author Chenggong Lu
 * @author Jeff Turner
 */
public class LinkFormatRenderer implements MercurialLinkRenderer
{
    private static Logger log = Logger.getLogger(LinkFormatRenderer.class);
    private String pathLinkFormat;
    private String fileReplacedFormat;
    private String fileAddedFormat;
    private String fileModifiedFormat;
    private String fileDeletedFormat;
    private String changesetFormat;

    public LinkFormatRenderer(MercurialManager mercurialManager)
    {

        ViewLinkFormat linkFormat = mercurialManager.getViewLinkFormat();
        if (linkFormat != null)
        {
            if (linkFormat.getChangesetFormat() != null
                    && linkFormat.getChangesetFormat().trim().length() != 0)
            {
                changesetFormat = linkFormat.getChangesetFormat();
            }

            if (linkFormat.getFileAddedFormat() != null
                    && linkFormat.getFileAddedFormat().trim().length() != 0)
            {
                fileAddedFormat = linkFormat.getFileAddedFormat();
            }

            if (linkFormat.getFileModifiedFormat() != null
                    && linkFormat.getFileModifiedFormat().trim().length() != 0)
            {
                fileModifiedFormat = linkFormat.getFileModifiedFormat();
            }

            if (linkFormat.getFileReplacedFormat() != null
                    && linkFormat.getFileReplacedFormat().trim().length() != 0)
            {
                fileReplacedFormat = linkFormat.getFileReplacedFormat();
            }

            if (linkFormat.getFileDeletedFormat() != null
                    && linkFormat.getFileDeletedFormat().trim().length() != 0)
            {
                fileDeletedFormat = linkFormat.getFileDeletedFormat();
            }

            if (linkFormat.getViewFormat() != null
                    && linkFormat.getViewFormat().trim().length() != 0)
            {
                pathLinkFormat = linkFormat.getViewFormat();
            }

        }
        else
        {
            log.warn("viewLinkFormat is null");
        }
    }

    public String getCopySrcLink(HGLogEntry revision, HGLogEntryPath logEntryPath)
    {
        long revisionNumber = revision.getShortRevision();
        return linkPath(pathLinkFormat, logEntryPath.getCopyPath(), revisionNumber);
//                getPathLink(logEntryPath.getCopyPath());
    }

    public String getRevisionLink(HGLogEntry revision)
    {
        return getRevisionLink(revision.getShortRevision());
    }

    public String getChangePathLink(HGLogEntry revision, HGLogEntryPath logEntryPath)
    {
        char changeType = logEntryPath.getType();
        String path = logEntryPath.getPath();
        long revisionNumber = revision.getShortRevision();

        if (changeType == MercurialConstants.MODIFICATION)
        {
            return linkPath(fileModifiedFormat, path, revisionNumber);
        }
        else if (changeType == MercurialConstants.ADDED)
        {
            return linkPath(fileAddedFormat, path, revisionNumber);
        }
        else if (changeType == MercurialConstants.REPLACED)
        {
            return linkPath(fileReplacedFormat, path, revisionNumber);
        }
        else if (changeType == MercurialConstants.DELETED)
        {
            return linkPath(fileDeletedFormat, path, revisionNumber);
        }
        else
        {
            return linkPath(fileReplacedFormat, path, revisionNumber);
        }
    }

    public String getRevisionLink(long revisionNumber)
    {
        if (changesetFormat != null)
        {
            try
            {
                String href = StringUtils.replaceAll(changesetFormat, "${rev}", "" + revisionNumber);
                return "<a href=\"" + href + "\">#" + revisionNumber + "</a>";
            }
            catch (Exception ex)
            {
                log.error("format error: " + ex.getMessage(), ex);
            }
        }
        return "#" + revisionNumber;

    }

    private String linkPath(final String format, String path, long revisionNumber)
    {
        if (format != null)
        {

            try
            {
                String href = format;
                if (path != null)
                {
                    href = StringUtils.replaceAll(href, "${path}", path);
                }
                href = StringUtils.replaceAll(href, "${rev}", "" + revisionNumber);
                href = StringUtils.replaceAll(href, "${rev-1}", "" + (revisionNumber - 1));

                return "<a href=\"" + href + "\">" + path + "</a>";
            }
            catch (Exception ex)
            {
                log.error("format error: " + ex.getMessage(), ex);
            }
        }
        return path;
    }
}
