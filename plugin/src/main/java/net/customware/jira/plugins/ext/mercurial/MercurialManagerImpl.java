/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Sep 30, 2004
 * Time: 8:13:56 AM
 */
package net.customware.jira.plugins.ext.mercurial;

import net.customware.jira.plugins.ext.mercurial.linkrenderer.LinkFormatRenderer;
import net.customware.jira.plugins.ext.mercurial.linkrenderer.MercurialLinkRenderer;
import net.customware.hg.core.io.IHGLogEntryHandler;
import net.customware.hg.core.io.HGException;
import net.customware.hg.core.io.HGLogEntry;
import net.customware.hg.core.auth.BasicAuthenticationManager;
import net.customware.hg.core.io.HGRepository;
import net.customware.hg.core.io.HGRepositoryLocation;

import com.atlassian.jira.InfrastructureException;
import com.atlassian.jira.util.JiraKeyUtils;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.util.TextUtils;

import org.apache.commons.collections.map.LRUMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class MercurialManagerImpl implements MercurialManager {
    private static Logger log = Logger.getLogger(MercurialManagerImpl.class);

    private MercurialLinkRenderer linkRenderer;
    private Map logEntryCache;
    private HGRepository repository;

    private boolean active;
    private boolean subrepos;
    private String inactiveMessage;
    private long id; // not the repository UUID
    private PropertySet properties;

    private ViewLinkFormat viewLinkFormat = null;
    private boolean isViewLinkSet = false;

    public MercurialManagerImpl(long id, PropertySet props, boolean isSubrepos) {
        this.id = id;
        this.properties = props;
        this.subrepos = isSubrepos;
        setup();
    }

    public synchronized void update(HgProperties props) {
        deactivate(null);

        HgProperties.Util.fillPropertySet(props, properties);
        isViewLinkSet = false; /* If we don't reset this flag, we get issue SVN-190 */

        setup();
    }

    protected void setup() {
        // Now setup web link renderer
        linkRenderer = null;

        if (getViewLinkFormat() != null) {
            linkRenderer = new LinkFormatRenderer(this);
        }

        // Now setup revision indexing if they want it
        if (isRevisionIndexing()) {
            // Setup the log message cache
            int cacheSize = 10000;

            if (getRevisioningCacheSize() > 0) {
                cacheSize = getRevisioningCacheSize();
            }

            logEntryCache = new LRUMap(cacheSize);
        } else {
            logEntryCache = new LRUMap();
        }

        activate();
    }

    /**
     * Return the HGLogEntry objects that are new since the given revision.
     */
    public synchronized Collection getLogEntries(long revision) {
        final Collection logEntries = new ArrayList();

        // if connection isn't up, don't even try
        if (!isActive()) {
            return logEntries;
        }

        // TODO not until hg log -v works on subrepositories
        if (isSubrepos()) {
            return logEntries;
        }

        long latestRevision;
        try {
            // This calls hg pull
            latestRevision = repository.getLatestRevision();
        } catch (HGException e) {
            // The connection was active, but apparently now it's not
            log.error("Error getting the latest revision from the repository: " + e.getMessage());
            deactivate(e.getMessage());
            return logEntries;
        }

        if (latestRevision == -1) {
            // We've done an hg pull and hg tip is still -1
            String msg = "Repository is empty";
            deactivate(msg);
            return logEntries;
        }

        String revStr = "" + revision;
        if (revision == -1) {
            revStr = "unknown";
        }
        log.debug("Latest actual revision in repository " + getRoot() + " is " + latestRevision + " and the previous known revision is " + revStr);

        if (latestRevision <= revision) {
            log.debug("All the changesets for repository " + getRoot() + " are present");
            return logEntries;
        }

        // A repo with just one changeset requires -r0:0
        // A single changeset difference requires -rN:N
        // A two changeset difference requires -rN-1:N
        long retrieveStart = revision + 1;
        if (retrieveStart > latestRevision) {
            retrieveStart = latestRevision;
        }

        // A full reindex of every remote repository will likely take about
        // the same time as running a regular update, showing that the
        // bottleneck is not JIRA
        log.debug("Retrieving revisions between " + retrieveStart + " and " + latestRevision + " inclusive for repository " + getRoot());

        try {
            repository.runHgLog(new String[]{""}, retrieveStart, latestRevision, true, true, new IHGLogEntryHandler() {
                public void handleLogEntry(HGLogEntry logEntry) {
                    log.debug("Retrieved #" + logEntry.getShortRevision() + " : " + logEntry.getMessage());
                    ensureCached(logEntry);

                    // If the message is used to detect releases, then
                    // we have to index all changesets, not just the
                    // ones with issue ids
                    //Prior: if (TextUtils.stringSet(logEntry.getMessage()) && JiraKeyUtils.isKeyInString(StringUtils.upperCase(logEntry.getMessage()))) {
                    if (TextUtils.stringSet(logEntry.getMessage())) {
                        logEntries.add(logEntry);
                    }
                }
            });
        }
        catch (HGException e) {
            log.warn("Problem retrieving changes from the repository " + getDisplayName() + " : " + e.getMessage());
            deactivate(e.getMessage());
        }

        log.debug("Retrieved " + logEntries.size() + " relevant revisions to index (" + retrieveStart + " to " + latestRevision + " inclusive) from repository " + getRoot());
        log.debug("log entries size = " + logEntries.size() + " for " + getRoot());
        return logEntries;
    }

    /**
     * This provides a cached front end to HGLogEntry objects. This
     * cache is rebuilt from the index on disk every time JIRA is
     * restarted and the indexer service runs or a release notes
     * report is requested. There is a separate cache in memory for
     * each repository.
     */
    public synchronized HGLogEntry getLogEntry(long revision) {
        if (!isActive()) {
            throw new IllegalStateException("The connection to the repository is not active");
        }

        // More efficient to parse all the revisions in one go at start up
        // TODO use the latest known revision here instead of revision
        // but have to synchronized with getLogEntries and it calls hg
        // pull. Could use hg tip by itself?
        if (logEntryCache.isEmpty()) {
            try {
                log.debug("Running a bulk hg log to populate the cache up to revision " + revision);
                repository.runHgLog(new String[]{""}, 0, revision, true, true, new IHGLogEntryHandler() {
                        public void handleLogEntry(HGLogEntry entry) {
                            ensureCached(entry);
                        }
                    });
            } catch (HGException e) {
                log.error("Error retrieving all the log messages: " + e, e);
                throw new InfrastructureException(e);
            }
        }

        final HGLogEntry[] logEntry = new HGLogEntry[]{(HGLogEntry) logEntryCache.get(new Long(revision))};

        if (logEntry[0] == null) {
            try {
                log.debug("No cache - retrieving the log message for revision: " + revision);
                repository.runHgLog(new String[]{""}, revision, revision, true, true, new IHGLogEntryHandler() {
                        public void handleLogEntry(HGLogEntry entry) {
                            logEntry[0] = entry;
                            ensureCached(entry);
                        }
                    });
            }
            catch (HGException e) {
                log.error("Error retrieving log messages: " + e, e);
                deactivate(e.getMessage());
                throw new InfrastructureException(e);
            }
        } else {
            log.debug("Found cached log message for revision: " + revision);
        }
        return logEntry[0];
    }

    public long getId() {
        return id;
    }

    /**
     * Make sure a single log message is cached.
     */
    private void ensureCached(HGLogEntry logEntry) {
        synchronized (logEntryCache) {
            logEntryCache.put(new Long(logEntry.getShortRevision()), logEntry);
        }
    }

    public PropertySet getProperties() {
        return properties;
    }

    public String getDisplayName() {
        return !properties.exists(MultipleMercurialRepositoryManager.HG_REPOSITORY_NAME) ? getRoot() : properties.getString(MultipleMercurialRepositoryManager.HG_REPOSITORY_NAME);
    }

    public String getRoot() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_ROOT_KEY);
    }

    public String getReleasenotesemail() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_RELEASENOTESEMAIL_KEY);
    }

    public String getReleasenoteslatest() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_RELEASENOTESLATEST_KEY);
    }

    public String getClonedir() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_CLONEDIR_KEY);
    }

    public String getSubreposcmd() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_SUBREPOSCMD_KEY);
    }

    public String getUsername() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_USERNAME_KEY);
    }

    public String getPassword() {
        try {
            return decryptPassword(properties.getString(MultipleMercurialRepositoryManager.HG_PASSWORD_KEY));
        } catch(IOException e) {
            log.error("Couldn't decrypt the password. Reseting it to null.", e);
            return null;
        }
    }

    public boolean isSubrepos() {
        return subrepos;
    }

    public String getRepositoryUUID() {
        // Note that this uses the underlying repository object not the properties.
        if (repository != null) {
            return repository.getRepositoryUUID();
        }
        return "";
    }

    public boolean isRevisionIndexing() {
        return properties.getBoolean(MultipleMercurialRepositoryManager.HG_REVISION_INDEXING_KEY);
    }

    public int getRevisioningCacheSize() {
        return properties.getInt(MultipleMercurialRepositoryManager.HG_REVISION_CACHE_SIZE_KEY);
    }

    public String getPrivateKeyFile() {
        return properties.getString(MultipleMercurialRepositoryManager.HG_PRIVATE_KEY_FILE);
    }

    public boolean isActive() {
        return active;
    }

    public String getInactiveMessage() {
        return inactiveMessage;
    }

    /**
     * A repository is active if it has been cloned and the connection is valid.
     * A subrepositories manager just needs to exist.
     */
    public void activate() {
        if (isSubrepos()) {
            activate_subrepos();
            return;
        }

        try {
            HGRepositoryLocation location = HGRepositoryLocation.parseURL(getRoot());
            repository = new HGRepository(location, getClonedir());

            if (!repository.checkCloneDir()) {
                String msg = "Unable to create the clone directory: " + getClonedir();
                log.error(msg);
                active = false;
                inactiveMessage = msg;
                return;
            }
            log.debug("Clone dir exists");

            if (!repository.testConnection()) {
                String msg = "Unable to connect to the repository at: " + location;
                log.error(msg);
                active = false;
                inactiveMessage = msg;
                // TODO possibly delete the clonedir?
                return;
            }
            log.debug("Connected");

            // TODO Now we have a valid repository UUID and can check for duplicates
            if (false) {
                //repository.getRepositoryUUID();
                String msg = "Duplicate repository at: " + location;
                log.error(msg);
                active = false;
                inactiveMessage = msg;
                // TODO possibly delete the clonedir?
                return;
            }
            log.debug("Unique");
          
            if (!repository.checkRepoCloneDir()) {
                String msg = "Unable to clone the repository: " + location;
                log.error(msg);
                active = false;
                inactiveMessage = msg;
                return;
            }
            log.debug("Repo cloned");

            active = true;
        }
        catch (HGException e) {
            log.error("Activating the Mercurial repository " + getRoot() + " failed: " + e, e);
            // We don't want to throw an exception here because then the system won't start if the repo is down
            // or there is something wrong with the configuration.  We also still want this repository to show up
            // in our configuration so the user has a chance to fix the problem.
            active = false;
            inactiveMessage = e.getMessage();
        }
    }

    /**
     * A subrepository is active if it was found
     */
    public void activate_subrepos() {
        // Note that the location is not parsed and repository is not set
        log.info("Activating a repository that has subrepositories");
        // TODO check that the command worked?
        active = true;
    }

    public void deactivate(String message) {
        if (repository != null) {
            repository = null;
        }
        active = false;
        inactiveMessage = "";
        if (message != null) {
            inactiveMessage = message;
        }
    }

    public ViewLinkFormat getViewLinkFormat() {
        if (!isViewLinkSet) {
            final String type = properties.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_TYPE);
            final String linkPathFormat = properties.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_PATH_KEY);
            final String changesetFormat = properties.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_CHANGESET);
            final String fileModifiedFormat = properties.getString(MultipleMercurialRepositoryManager.HG_LINKFORMAT_FILE_MODIFIED);
            final String fileAddedFormat = "unused";
            final String fileReplacedFormat = "unused";
            final String fileDeletedFormat = "unused";

            if (linkPathFormat != null || changesetFormat != null || fileAddedFormat != null || fileModifiedFormat != null || fileReplacedFormat != null || fileDeletedFormat != null)
                viewLinkFormat = new ViewLinkFormat(type, changesetFormat, fileAddedFormat, fileModifiedFormat, fileReplacedFormat, fileDeletedFormat, linkPathFormat);
            else
                viewLinkFormat = null; /* [issue SVN-190] This could happen if the user clears all the fields in the Mercurial repository web link configuration */
            isViewLinkSet = true;
        }

        return viewLinkFormat;
    }

    public MercurialLinkRenderer getLinkRenderer() {
        return linkRenderer;
    }


    protected static String decryptPassword(String encrypted) throws IOException {
        if (encrypted == null)
            return null;
        byte[] result = Base64Coder.decode(encrypted);
        return new String(result, 0, result.length);
    }

    protected static String encryptPassword(String password) {
        if (password == null)
            return null;
        return new String(Base64Coder.encode(password.getBytes()));
    }

    public void recordRelease(String artifactId, String label) {
        log.warn("Setting the latest release label for " + artifactId + " to: " + label);
        // TODO this assumes only one artifact per repository
        properties.setString(MultipleMercurialRepositoryManager.HG_RELEASENOTESLATEST_KEY, label);
    }

}
