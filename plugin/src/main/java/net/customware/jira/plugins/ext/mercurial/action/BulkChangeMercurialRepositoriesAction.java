package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;

import java.util.*;

/**
 * A class to support changing only selected Mercurial Repositories.
 */
public class BulkChangeMercurialRepositoriesAction extends MercurialActionSupport {
    private String repoIds;
    
    public BulkChangeMercurialRepositoriesAction() {
    }
    
    /**
     * Activate all selected repositories
     */
    public String doActivate() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        if (getRepoids() == null) {
            addErrorMessage("No repositories were selected for activation");
            return "activateall";
        }

        List<MercurialManager> mercurialManagers = getMercurialManagers();
        for (MercurialManager mm: mercurialManagers) {
            if (!mm.isActive()) {
                mm.activate();
                if (!mm.isActive()) {
                    addErrorMessage(getText("mercurial.repository.activation.failed", mm.getDisplayName()));
                }
            }
        }
        
        return "activateall";
    }
    
    public String doDeactivate() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        if (getRepoids() == null) {
            addErrorMessage("No repositories were selected for deactivation");
            return "deactivateall";
        }

        List<MercurialManager> mercurialManagers = getMercurialManagers();
        for (MercurialManager mm: mercurialManagers) {
            mm.deactivate("explicitly deactivated");
            if (mm.isActive()) {
                addErrorMessage(getText("mercurial.repository.deactivation.failed"));
            }
        }
        
        return "deactivateall";
    }

    public String doReindex() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        if (getRepoids() == null) {
            addErrorMessage("No repositories were selected for reindexing");
            return "reindexall";
        }

        List<MercurialManager> mercurialManagers = getMercurialManagers();
        for (MercurialManager mm: mercurialManagers) {
            try {
                getMultipleRepoManager().reindexRepository(mm.getId());
            } catch (Exception ex) {
                log.warn("Unable to reindex repository " + mm.getDisplayName() + ": " + ex.getMessage());
                continue;
            }
        }
        
        return "reindexall";
    }

    public String doDelete() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        if (getRepoids() == null) {
            addErrorMessage("No repositories were selected for deletion");
            return "deleteall";
        }

        List<MercurialManager> mercurialManagers = getMercurialManagers();
        for (MercurialManager mm: mercurialManagers) {
            getMultipleRepoManager().removeRepository(mm.getId());
            // TODO do deletes never fail?
        }
        
        return "deleteall";
    }

    /**
     * @return the managers for all the selected repositories
     */
    private List<MercurialManager> getMercurialManagers() {
        List<MercurialManager> result = new ArrayList<MercurialManager>();
        String text = getRepoids();
        if (text == null || text.equals("")) {
            log.debug("Ignoring empty repoids text");
            return result;
        }
        String[] repositories = text.split(",");
        String prefix = "repo_";
        for (String repoId: repositories) {
            if (repoId.length() <= prefix.length()) {
                log.error("Unable to parse a repoid from the string " + repoId + ", which came from splitting " + text);
                continue;
            }
            repoId = repoId.substring(prefix.length());
            MercurialManager mm = getMultipleRepoManager().getRepository(Long.parseLong(repoId));
            result.add(mm);
        }
        return result;
    }

    public String getRepoids() {
        return repoIds;
    }
    
    /**
     * The format is repo_NNN,repo_NNA
     */
    public void setRepoids(String repoIds) {
        log.debug("Setting the repository ids: "+ repoIds);
        this.repoIds = repoIds;
    }
    
    public String doDefault() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        // TODO check that all the repositories exist
        return INPUT;
    }
    
    public String doExecute() {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        // Do nothing at the moment
        return getRedirect("ViewMercurialRepositories.jspa");
    }
    
}
