/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Sep 30, 2004
 * Time: 1:44:30 PM
 */
package net.customware.jira.plugins.ext.mercurial.linkrenderer;

import net.customware.hg.core.io.HGLogEntryPath;
import net.customware.hg.core.io.HGLogEntry;

public interface MercurialLinkRenderer
{
    String getRevisionLink(HGLogEntry revision);

    String getRevisionLink(long revision);

    String getChangePathLink(HGLogEntry revision, HGLogEntryPath changePath);

    public String getCopySrcLink(HGLogEntry revision, HGLogEntryPath changePath);
}
