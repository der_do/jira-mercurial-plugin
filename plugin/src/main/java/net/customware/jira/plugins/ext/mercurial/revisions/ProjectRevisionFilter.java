package net.customware.jira.plugins.ext.mercurial.revisions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Filter;
import org.apache.lucene.util.OpenBitSet;

import java.io.IOException;

public class ProjectRevisionFilter extends AbstractRevisionFilter
{
    private final String projectKey;

    public ProjectRevisionFilter(IssueManager issueManager, PermissionManager permissionManager, User user, String projectKey)
    {
        super(issueManager, permissionManager, user);
        this.projectKey = projectKey;
    }
    
    @Override
    public DocIdSet getDocIdSet(IndexReader reader) throws IOException {
		OpenBitSet bitSet = new OpenBitSet(reader.maxDoc());
		
		TermDocs termDocs = reader.termDocs(new Term(RevisionIndexer.FIELD_PROJECTKEY, projectKey));
		while (termDocs.next()) {
			int docId = termDocs.doc();
			Document theDoc = reader.document(docId, issueKeysFieldSelector);

			boolean allow = false;
			String[] issueKeys = theDoc.getValues(RevisionIndexer.FIELD_ISSUEKEY);

			if (null != issueKeys) {
				for (String issueKey : issueKeys) {
					Issue anIssue = issueManager.getIssueObject(StringUtils.upperCase(issueKey));
					if (null != anIssue && permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, anIssue, user)) {
						allow = true;
						break;
					}
				}
			}
			bitSet.set(docId);
		}
		return bitSet;
    }
}
