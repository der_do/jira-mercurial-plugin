package net.customware.jira.plugins.ext.mercurial.action;

import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.WebLinkType;
import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexService;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Base class for the Mercurial plugins actions.
 */
public class MercurialActionSupport extends JiraWebActionSupport {

    private MultipleMercurialRepositoryManager multipleRepoManager;
    private List webLinkTypes;
    
    public MercurialActionSupport() {
        this.multipleRepoManager = RevisionIndexService.getMultipleMercurialRepositoryManager();
    }
    
    protected MultipleMercurialRepositoryManager getMultipleRepoManager() {
        return multipleRepoManager;
    }
    
    public boolean hasPermissions() {
        return isHasPermission(Permissions.ADMINISTER);
    }
    
    public String doDefault() {
        if (!hasPermissions())
        {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        return INPUT;
    }

    public List getWebLinkTypes() throws IOException {
        if (webLinkTypes == null) {
            webLinkTypes = new ArrayList();
            Properties properties = new Properties();
            properties.load(getClass().getResourceAsStream("/weblinktypes.properties"));
            
            String[] types = properties.getProperty("types", "").split(" ");
            for (int i = 0; i < types.length; i++) {
                webLinkTypes.add(new WebLinkType(types[i],
                                                 properties.getProperty(types[i] + ".name", types[i]),
                                                 properties.getProperty(types[i] + ".changeset"),
                                                 properties.getProperty(types[i] + ".file.modified")
                                                 ));
            }
        }
        return webLinkTypes;
    }
    
    public String escapeJavaScript(String javascriptUnsafeString)
    {
        return StringEscapeUtils.escapeJavaScript(javascriptUnsafeString);
    }
}
