/*
 * ====================================================================
 * Copyright (c) 2004 TMate Software Ltd.  All rights reserved.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at http://tmate.org/svn/license.html.
 * If newer versions of this license are posted there, you may use a
 * newer version instead, at your option.
 * ====================================================================
 */

package net.customware.hg.core.io;

import net.customware.hg.util.TimeUtil;
import net.customware.hg.util.ExecUtil;

import java.io.*;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import com.atlassian.jira.util.PathUtils;

import org.apache.log4j.Logger;

/**
 * @author Matthew Doar
 */
public class HGRepository {
        
    private String myRepositoryUUID = "not set";
    private String myRepositoryRoot = "not set";
    private HGRepositoryLocation myLocation;
    private String myCloneDir = "not set";

    public String toString() {
        return myRepositoryRoot + "(" + myRepositoryUUID + ") " + myLocation.toString() + ", clone dir: " + myCloneDir;  
    }

    private static Logger log = Logger.getLogger(HGRepository.class);

    public HGRepository(HGRepositoryLocation location, String cloneDir) {
        myLocation = location;
	myRepositoryRoot = location.toCanonicalForm();
	myCloneDir = cloneDir;
	log.debug("mercurial repository object: " + myRepositoryRoot + ", cloneDir: " + cloneDir);

	// This is not set here but instead is set by testConnection
        // myRepositoryUUID = myRepositoryRoot;
    }
    
    public HGRepositoryLocation getLocation() {
        return myLocation;
    }
    
    public String getRepositoryUUID() {
        return myRepositoryUUID;
    }
    
    public String getRepositoryRoot() {
        return myRepositoryRoot;
    }

    public String getClonedir() {
        return myCloneDir;
    }
    
    public boolean checkCloneDir() {
        // Check if clone directory is null.  If this is null it could
        // be a symptom of another problem.
        if (myCloneDir == null) {
            return false;
        }
        File dir = new File(myCloneDir);
        if (!dir.exists()) {
            log.info("Creating the directory " + myCloneDir);
            if (!dir.mkdir()) {
                log.error("Unable to create the directory " + myCloneDir);
                return false;
            }
        }

        if (!dir.canWrite()) {
            log.error("Unable to write to " + myCloneDir);
            return false;
        }

        return true;
    }

    public boolean checkRepoCloneDir() {
        String repoCloneDir = PathUtils.joinPaths(myCloneDir, myLocation.getLast());
        
        File dir = new File(repoCloneDir);
        if (!dir.exists()) {
            log.info("Creating a local clone of " + myLocation + " in " + myCloneDir);
            ExecUtil eu = new ExecUtil();
            String[] env_vars = new String[]{};
        
            try {
                // -U for no update
                eu.exec("hg clone -U " + myRepositoryRoot, env_vars, new File(myCloneDir), log);
                log.info("Cloning of " + myLocation + " is complete");
            } catch (IOException ioe) {
                log.error("Failed to clone repository: " + myRepositoryRoot);
                return false;
            }
        }
        return true;
    }

    /**
     * Run an hg command. All calls to this start in
     * MercurialManagerImpl and check that the repo is active first.
     *
     * For the latest revision: hg head
     *
     * For logging: update the local repository then run hg -v log on
     * it and return the result.
     */
    private ArrayList<String> hg_cmd(String cmd) throws HGException {
	ArrayList<String> linesList;
	String[] env_vars = new String[]{};
	ExecUtil eu = new ExecUtil();

        String repoCloneDir = PathUtils.joinPaths(myCloneDir, myLocation.getLast());
        File dir = new File(repoCloneDir);
        log.debug("cmd = " + cmd); // TODO debug
        if (cmd.equals("head")) {
            // Do an hg pull, then use hg tip to get the revision
            try {
                // no -u, so no update. Opposite meaning of u/U than hg clone!
                ArrayList<String> unused = eu.exec("hg pull", env_vars, dir, log);
            } catch (IOException ioe) {
                // added log statement to show what the actual error
                // that was raised by running the hg command
                log.error("unable to run hg pull command = env_vars: " + env_vars + " | dir: " + dir + " ", ioe);
                throw new HGException("unable to run hg pull command");
            }
            
            try {
                linesList = eu.exec("hg tip", env_vars, dir, log);
            } catch (IOException ioe) {
                throw new HGException("unable to run hg tip");
            }
        } else if (cmd.startsWith("log")) {
            try {
                linesList = eu.exec("hg " + cmd, env_vars, dir, log);
            } catch (IOException ioe) {
                throw new HGException("unable to run hg " + cmd);
            }
        } else {
            throw new HGException("Unknown command: " + cmd);
        }
	return linesList;
    }

    /**
     * This method uses hg pull to update the local clone.
     * The latest revision is returned, -1 if the repository is empty.
     */
    public long getLatestRevision() throws HGException {
	log.debug("Entered getLatestRevision");
	
	ArrayList<String> linesList = hg_cmd("head");
        String[] lines = linesList.toArray(new String[linesList.size()]);

	String cs_line = lines[0];
	if (cs_line.startsWith("changeset:   ")) {
	    String latest_revision = cs_line.substring(13).split(":")[0];
	    log.debug("latest revision = " + latest_revision);
            return Long.parseLong(latest_revision);
	} else {
	    throw new HGException("Unexpected hg log entry: "+ cs_line);
	}
    }

    /**
     * Run "hg log" and return the number of log entries found. Pass
     * the actual entries back using the handler.
     */
    public int runHgLog(String[] targetPaths, 
                        long startRevision, 
                        long endRevision, 
                        boolean changedPath, 
                        boolean strictNode,
                        IHGLogEntryHandler handler) throws HGException {
	log.debug("Entered hglog() with startRevision " + startRevision + 
		  ", endRevision " + endRevision);
	
        // Note that the results are in reverse order
	ArrayList<String> linesList = hg_cmd("log -v -r" + endRevision + ":" + startRevision);
        String[] lines = linesList.toArray(new String[linesList.size()]);

        int count = 0;

        // If you add a field here, make sure it is zeroed after being used
        // in the main loop
	String changesetShort = null;
	String changesetFull = null;
	String author = null;
	String tags = null;
        String branch = "default";
	Date date = null;
	String files = null;
	StringBuilder description = null;
	Map changedPathsMap = new HashMap();
	
	int i = 0;
        while(i < lines.length) {
            log.debug("Parsing line " + i + " : |" + lines[i] + "|");
            if (lines[i].equals("")) {
		// Ignore blank lines between entries
	    } else if (lines[i].startsWith("changeset:   ")) {
                String changeset = lines[i].substring(13);
		String[] cs = changeset.split(":");
		changesetShort = cs[0];
		changesetFull = cs[1];
            } else if (lines[i].startsWith("user:        ")) {
                author = lines[i].substring(13);
                int lt_idx = author.indexOf("<");
                int at_idx = author.indexOf("@");
                // Handle names like jsmith@bar.com and J Smith <jsmith@bar.com>
                // Find the smallest positive idx value
                int split_idx = -1;
                if (lt_idx != -1) {
                    split_idx = lt_idx;
                    if (at_idx != -1 && at_idx < lt_idx) {
                        split_idx = at_idx;
                    }
                } else {
                    if (at_idx != -1) {
                        split_idx = at_idx;
                    }
                }
                if (split_idx != -1) {
                    author = author.substring(0, split_idx);
                }
            } else if (lines[i].startsWith("files:       ")) {
		// The filenames appear on one line separated by spaces
		files = lines[i].substring(13);
		changedPathsMap = new HashMap();
		String[] s = files.split(" ");
		for (int j = 0; j < s.length; j++) {
		    /*
		      Change type is one of (M)odified, (A)dded,
		      (D)eleted, or (R)eplaced but hg -v log doesn't show
		      this information
		    */
		    String filename = s[j];
		    changedPathsMap.put(filename, new HGLogEntryPath(filename, 'M'));
		}
            } else if (lines[i].startsWith("parent:      ")) {
                // TODO if more that one parent, could use to indicate
                // this was a merge. Ignore for now.
            } else if (lines[i].startsWith("tag:         ")) {
                // There is one of these lines for each tag and the
                // appear with or without -v
		String tag = lines[i].substring(13);
                if (tags == null) {
                    tags = tag;
                } else {
                    // Assumes that tags never have a comma in them
                    tags += "," + tag;
                }
            } else if (lines[i].startsWith("date:        ")) {
		String dateStr = lines[i].substring(13);
		String no_tz = dateStr.substring(0,24);
		String timezone = dateStr.substring(25);
		date = TimeUtil.parseDate(no_tz, timezone);
            } else if (lines[i].startsWith("branch:      ")) {
                branch = lines[i].substring(13);
            } else if (lines[i].equals("description:")) {
                description = new StringBuilder();
                i++;
                // Handle the case of a description with the string "changeset:"
                // at the start of a line. Changesets are in decreasing order.
                int eSCS = Integer.parseInt(changesetShort) - 1;
                String expectedShortCS = "changeset:   " + eSCS;
                while (i < lines.length && !lines[i].startsWith(expectedShortCS)) {
		    log.debug("Adding description line " + i + ": |" + lines[i] + "|");
                    description.append(lines[i++]);
                    description.append("\n");
                }

		/* The description is the final field of an entry */
                /* It is perfectly reasonable for the files line to not be
                 * present, in the case of branch merges */
		if (changesetShort == null || changesetFull == null || 
		    author == null || date == null || 
		    description == null) {
		    throw new HGException("incomplete log record: changesetShort=" + 
    changesetShort + ", changesetFull=" + changesetFull + ", author=" + author + ", date=" + date + ", files=" + files + ", tags= " + tags + ", branch=" + branch + ", description " + description);
                }		
		HGLogEntry dummyData = new HGLogEntry(changedPathsMap, Long.parseLong(changesetShort), changesetFull, author, date, tags, branch, description.toString());
		handler.handleLogEntry(dummyData);
		count++;
		log.debug("Added a new changeset: " + changesetShort);
		i--;
		//log.debug("Current line: " + i);
		changesetShort = null;
		changesetFull = null;
		author = null;
                tags = null;
		files = null;
		description = null;
                branch = "default";
            } else {
		throw new HGException("Failed to parse hg log output for " + this.toString() + ".\nUnknown header on line " + (i+1) + ": " + lines[i]);
            }
	    i++;
	}
	return count;
    }

    public boolean testConnection() throws HGException {
	log.debug("Entered testConnection");
        // TODO subrepositories don't work with identify yet

        boolean identifyIsSlow = true;
        if (identifyIsSlow) {
            // This means that repositories will not be declared
            // inactive until the next time the index service runs
            myRepositoryUUID = myRepositoryRoot;
            return true;
        }

        ExecUtil eu = new ExecUtil();
        String[] env_vars = new String[]{};
        try {
            // This can take 10s on a slow network - dns, ssh, hg
            // delay but the results differ according to whether the
            // repository is referred to by a directory path or a URL
            // 000000000000 is returned for the directory path case
            // TODO another approach would be to run identify lazily
            //String cmd = "hg identify " + myCloneDir + System.getProperty("file.separator") + new File(myRepositoryRoot).getName();
            String cmd = "hg identify " + myRepositoryRoot;
            ArrayList<String> linesList = eu.exec(cmd, env_vars, new File(myCloneDir), log);
            String[] lines = linesList.toArray(new String[linesList.size()]);
            log.debug("hg identify returned: " + lines[0]);
            if (lines.length > 0) {
                // This summary identifies the repository state using
                // one or two parent hash identifiers, followed by a
                // "+" if there are uncommitted changes in the working
                // directory, a list of tags for this revision and a
                // branch name for non-default branches.
                myRepositoryUUID = lines[0];
            }
        } catch (IOException ioe) {
            log.error("Failed to connect to and identify repository: " + myRepositoryRoot);
            log.debug(ioe.getMessage());
            return false;
        }

        return true;
    }
 
    /**
     * This is here to make testing parsing logs easier.
     *
     * cd target/classes
     * java -cp .:/jira/atlassian-jira-enterprise-4.0.1-standalone/atlassian-jira/WEB-INF/lib/log4j-1.2.15.jar net.customware.hg.core.io.HGRepository
     */
    public static void main(String[] args) {
	int count = 0;
	try {
	    org.apache.log4j.BasicConfigurator.configure();
	    HGRepository currentRepo = new HGRepository(HGRepositoryLocation.parseURL("http://hg.mycompany.com/hg/repoA"), "/usr/local/hg/repos/repoB");
	    long latest = currentRepo.getLatestRevision();
	    log.debug("Latest revision is: " + latest);

	    count = currentRepo.runHgLog(new String[]{""}, 0, latest, true, true, new IHGLogEntryHandler()
		{
		    public void handleLogEntry(HGLogEntry logEntry)
		    {
			log.debug("Retrieved #" + logEntry.getShortRevision() + " : " + logEntry.getMessage());		    
		    }
		});
	} catch (Exception e) {
	    log.error("Exception: " + e);
	    e.printStackTrace();
	}
	log.info("End of test, found " + count + " changesets");
    }

}
