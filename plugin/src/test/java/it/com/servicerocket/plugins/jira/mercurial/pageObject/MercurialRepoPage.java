package it.com.servicerocket.plugins.jira.mercurial.pageObject;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Created by yasir on 4/16/14.
 */
public class MercurialRepoPage extends AbstractJiraAdminPage {

    @ElementBy(xpath = "//h3[text()='Mercurial Repositories']")
    PageElement title;

    @Override
    public String linkId() {
        return null;
    }

    @Override
    public TimedCondition isAt() {
        return title.timed().isVisible();
    }

    @Override
    public String getUrl() {
        return "/jira/secure/ViewMercurialRepositories.jspa";
    }

    public String getTitleText(){
        return title.getText();
    }
}
