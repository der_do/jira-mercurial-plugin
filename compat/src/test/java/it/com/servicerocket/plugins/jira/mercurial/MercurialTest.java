package it.com.servicerocket.plugins.jira.mercurial;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@Cucumber.Options(format = {"pretty", "html:target/steps"}, features="features/")
public class MercurialTest {
}
