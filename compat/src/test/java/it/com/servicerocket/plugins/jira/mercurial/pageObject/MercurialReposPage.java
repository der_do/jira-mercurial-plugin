package it.com.servicerocket.plugins.jira.mercurial.pageObject;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class MercurialReposPage extends AbstractJiraAdminPage {

    @ElementBy(xpath = "//h3[text()='Mercurial Repositories']")
    private PageElement title;

    @ElementBy(xpath = "//td[text()='MercurialTest']")
    private PageElement repo;

    @ElementBy(xpath = "//td[text()='Active']")
    private PageElement status;

    public boolean isRepoActive() {
        if (!repo.isPresent()) return false;
        return status.isPresent();
    }

    @Override
    public TimedCondition isAt() {
        return title.timed().isVisible();
    }

    @Override
    public String linkId() {
        return "net.customware.jira.plugins.mercurial.link";
    }

    @Override
    public String getUrl() {
        return "/secure/ViewMercurialRepositories.jspa";
    }
}
